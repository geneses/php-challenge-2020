<?php

namespace App\DAO;

require_once '../../vendor/autoload.php';

use PDO;
use App\DAO\{
    Conexao,
    Metodos
};

class Tipo_produtosDAO implements Metodos
{
  
    //Listar 
    public function list($id = null)
    {
        //verificar se a listagem é geral ou individual
        if(is_null($id)){
            $sql = "SELECT * FROM Tipo_produtos";
            $consulta = Conexao::getConexao()->prepare($sql);
        }else{
            $sql = "SELECT * FROM Tipo_produtos WHERE id = ?";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$id);
        }
        
        $consulta->execute(); 
        return $consulta->fetchAll(PDO::FETCH_OBJ);
    }

    //Cadastrar tipos
    public function create($objeto)
    {        
        
        
       try{
            $sql = "INSERT INTO Tipo_produtos(nome,porcentagem) VALUES(?,?)";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$objeto->getNome());
            $consulta->bindValue(2,$objeto->getPorcentagem());
            $consulta->execute();
        }catch(\PDOException $e){
            echo $e->getMessage();
        }
    }

    //atualizar tipos
    public function update($objeto)
    {
        try{
            $sql = "UPDATE Tipo_produtos SET nome = ?, porcentagem = ? WHERE id = ?";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$objeto->getNome());
            $consulta->bindValue(2,$objeto->getPorcentagem());
            $consulta->bindValue(3,$objeto->getId());
            $consulta->execute();
        }catch(\PDOException $e){
            echo $e->getMessage();
        }
    }

    //deletar tipos
    public function delete(int $id): bool
    {
        try{
            $sql = "DELETE FROM Tipo_produtos WHERE id = ?";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$id);
            $consulta->execute();
            //retornar caso tenha realizado a alteração com sucesso
            if($consulta->rowCount())
                return true;
            return false;
        }catch(\PDOException $e){
            echo $e->getMessage();
        }
    }

}



