<?php

namespace App\DAO;

use PDO;
class Conexao
{
    private static $conexao;

    //Abrir e retornar conexão com o banco
    public static function getConexao()
    {
        //Criar nova instância de conexão, caso não tenha
        if(!isset(self::$conexao)){
            try {
                self::$conexao = new PDO('mysql:host=mysql; dbname=geneses', 'root', 'root');
                self::$conexao->exec("set names utf8");
                self::$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        return self::$conexao;
    }

        
}

