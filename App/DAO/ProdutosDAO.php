<?php

namespace App\DAO;

require_once '../../vendor/autoload.php';

use PDO;
use App\DAO\{
    Conexao,
    Metodos
};

class ProdutosDAO implements Metodos
{
    
    //Listar produtos
    public function list($id = null)
    {
        try{
            if(is_null($id)){
                $sql = "SELECT Produtos.id, Produtos.nome, Tipo_produtos.nome as nome_tipo, Tipo_produtos.id as id_tipo_produto, Produtos.valor 
                FROM Produtos
                JOIN Tipo_produtos
                ON Produtos.tipo_produtos_id = Tipo_produtos.id";
                $consulta = Conexao::getConexao()->prepare($sql);
            }
            else{
                $sql = "SELECT Produtos.id, Produtos.nome, Tipo_produtos.nome as nome_tipo, Produtos.valor 
                FROM Produtos
                JOIN Tipo_produtos
                ON Produtos.tipo_produtos_id = Tipo_produtos.id
                WHERE Produtos.id = ?";
                $consulta = Conexao::getConexao()->prepare($sql);
                $consulta->bindValue(1,$id);
            }
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    //cadastrar produtos
    public function create($objeto)
    {
        
        try{
            $sql = "INSERT INTO Produtos(nome,tipo_produtos_id,valor) VALUES(?,?,?)";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$objeto->getNome());
            $consulta->bindValue(2,$objeto->getTipoProduto());
            $consulta->bindValue(3,$objeto->getValor());
            $consulta->execute();
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    //Atualizar produtos
    public function update($objeto)
    {
        try{
            $sql = "UPDATE Produtos SET nome = ?, tipo_produtos_id = ?, valor = ? WHERE id = ? ";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$objeto->getNome());
            $consulta->bindValue(2,$objeto->getTipoProduto());
            $consulta->bindValue(3,$objeto->getValor());
            $consulta->bindValue(4,$objeto->getId());
            $consulta->execute();
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    public function delete(int $id): bool
    {
        try{
            $sql = "DELETE FROM Produtos WHERE id = ?";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->bindValue(1,$id);
            $consulta->execute();
            //retornar caso tenha realizado a alteração com sucesso
            if($consulta->rowCount())
                return true;
            return false;
        }catch(\PDOException $e){
            echo $e->getMessage();
        }
    }

}
