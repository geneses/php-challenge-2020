<?php

namespace App\DAO;

//interface para métodos do crud
interface Metodos {
    //Criar
    public function create($objeto);
    //listar 
    public function list($id = null);
    //editar
    public function update($objeto);
    //deletar
    public function delete(int $id): bool;

}