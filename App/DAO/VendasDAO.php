<?php
namespace App\DAO;

require_once "../../vendor/autoload.php";

use PDO;
use App\DAO\{
    Conexao,
};


class VendasDAO
{

    //Listar Vendas
    public function list()
    {
        try{
            $sql = "SELECT Vendas.id, (Produtos.valor * Vendas.quantidade) as valor_quantidade, Produtos.valor, Tipo_produtos.porcentagem, (Tipo_produtos.porcentagem * Produtos.valor) / 100 as Valor_impostos, (Produtos.valor * Vendas.quantidade) + (Tipo_produtos.porcentagem * Produtos.valor) / 100 as Total_compras
            FROM Produto_vendas 
            JOIN Produtos ON Produto_vendas.Produtos_id = Produtos.id 
            JOIN Vendas ON Produto_vendas.Vendas_id = Vendas.id
            JOIN Tipo_produtos ON Produtos.tipo_produtos_id = Tipo_produtos.id";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    //list totalizador
    public function totalizador(){
        try{
            $sql = "SELECT SUM(Valor_impostos) as Total_impostos, SUM(Total_compras) as total_compras FROM(
                SELECT Vendas.id, (Produtos.valor * Vendas.quantidade) as valor_quantidade, Produtos.valor, Tipo_produtos.porcentagem, (Tipo_produtos.porcentagem * Produtos.valor) / 100 as Valor_impostos, (Produtos.valor * Vendas.quantidade) + (Tipo_produtos.porcentagem * Produtos.valor) / 100 as Total_compras
                    FROM Produto_vendas 
                    JOIN Produtos ON Produto_vendas.Produtos_id = Produtos.id 
                    JOIN Vendas ON Produto_vendas.Vendas_id = Vendas.id
                    JOIN Tipo_produtos ON Produtos.tipo_produtos_id = Tipo_produtos.id
                ) AS tabela_derivada";
            $consulta = Conexao::getConexao()->prepare($sql);
            $consulta->execute();
            return $consulta->fetchAll(PDO::FETCH_OBJ);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
        
    }

    //Criando vendas
    public function create($objeto)
    {
        try{
            $sql = "INSERT INTO Vendas(quantidade) VALUES(?)";
            $insert = Conexao::getConexao()->prepare($sql);
            $insert->bindValue(1,$objeto->getQuantidade());
            $insert->execute();
            $id_venda = Conexao::getConexao()->lastInsertId();
            
            $sql = "INSERT INTO Produto_vendas(Produtos_id,Vendas_id) VALUES(?,?)";
            $insert2 = Conexao::getConexao()->prepare($sql);
            $insert2->bindValue(1,$objeto->getProduto());
            $insert2->bindValue(2,$id_venda);
            $insert2->execute();
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

}
