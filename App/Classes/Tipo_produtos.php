<?php

/**
 * Classe responsável por realizar as validações dos campos
 */

namespace App\Classes;

require_once '../../vendor/autoload.php';

class Tipo_produtos
{
    private $nome;
    private $porcentagem;
    private $id;

    //encapsulamentos e validações

    public function getNome(){
        return $this->nome;
    }

    public function getPorcentagem(){
        return $this->porcentagem;
    }

    public function getId(){
        return $this->id;
    }

    public function setNome($dado){
        //verificar se é string
        if(!is_string($dado)){
            echo json_encode(['error' => 'O Nome precisa ser uma string'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        
        $this->nome = $dado;
    }

    public function setPorcentagem($dado){
        //verificando se é float
        if(!is_numeric($dado)){
            echo json_encode(['error' => 'A porcentagem precisa ser númerico'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->porcentagem = $dado;
    }

    public function setId($dado){
        //verificando se é inteiro
        if(!is_integer($dado)){
            echo json_encode(['error' => 'O Id precisa ser Inteiro'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->id = $dado;
    }
}
