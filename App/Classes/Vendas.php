<?php

/**
 * Classe responsável por realizar as validações dos campos
 */

namespace App\Classes;

require_once '../../vendor/autoload.php';

class Vendas
{
    private $quantidade;
    private $produto_id;
    

    //encapsulamentos e validações

    public function getQuantidade(){
        return $this->quantidade;
    }

    public function getProduto(){
        return $this->produto_id;
    }


    public function setQuantidade($dado){
        //verificar se é string
        if(!is_string($dado)){
            echo json_encode(['error' => 'O quantidade precisa ser númerico'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        
        $this->quantidade = $dado;
    }

    public function setProduto($dado){
        //verificando se é float
        if(!is_numeric($dado)){
            echo json_encode(['error' => 'A produto_id precisa '],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->produto_id = $dado;
    }

}