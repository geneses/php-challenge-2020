<?php

/**
 * Classe responsável por realizar as validações dos campos
 */

namespace App\Classes;

require_once '../../vendor/autoload.php';


class Produtos
{
    private $nome;
    private $valor;
    private $id;
    private $tipo_produtos_id;

    //encapsulamentos e validações

    public function getNome(){
        return $this->nome;
    }

    public function getValor(){
        return $this->valor;
    }

    public function getId(){
        return $this->id;
    }

    public function getTipoProduto(){
        return $this->tipo_produtos_id;
    }

    public function setNome($dado){
        //verificar se é string
        if(!is_string($dado)){
            echo json_encode(['error' => 'O Nome precisa ser uma string'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        
        $this->nome = $dado;
    }

    public function setValor($dado){
        //verificando se é float
        if(!is_numeric($dado)){
            echo json_encode(['error' => 'A valor precisa ser númerico'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->valor = $dado;
    }

    public function setId($dado){
        //verificando se é inteiro
        if(!is_integer($dado)){
            echo json_encode(['error' => 'O Id precisa ser Inteiro'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->id = $dado;
    }

    public function setTipoProduto($dado){
        //verificando se é inteiro
        if(!is_integer($dado)){
            echo json_encode(['error' => 'O Id do tipo de produto precisa ser Inteiro'],JSON_UNESCAPED_UNICODE);
            http_response_code(400);
            die();
        }
        $this->tipo_produtos_id = $dado;
    }
}
/*
$produto = new Produtos();
$produto->setNome('24');
$produto->setValor(23);
$produto->setTipoProduto(1);
$produto_dao = new ProdutosDAO();
$produto_dao->create($produto);
echo "<pre>";
var_dump($produto_dao->list());*/



