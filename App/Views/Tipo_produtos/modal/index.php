<div class="modal fade" id="modal_tipo_produto" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Cadastrar Modal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert" id="alerta" hidden>
        </div>
        <form id="form_tipo_produto" >
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="nome">nome</label>
                <input type="text" class="form-control-file" id="nome" name="nome">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="porcentagem">porcentagem</label>
                <input type="text" class="form-control-file" id="porcentagem" name="porcentagem">
              </div>
            </div>
            <input type="hidden" id="tipo_requisicao" value="" />
            
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar mudanças</button>
      </div>
      </form>
    </div>
  </div>
</div>