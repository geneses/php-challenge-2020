<?php

require_once '../../vendor/autoload.php';

use App\DAO\Tipo_produtosDAO;
use App\Classes\Tipo_produtos;

//Recuperando a operação do método
$url = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/')+28);
//Recuperando o método
$metodo = $_SERVER['REQUEST_METHOD'];

//tratar por tipo de requisição
switch($metodo){
    case 'GET':
        if($url === 'list'){
            $tipo = new Tipo_produtosDAO;
            echo json_encode($tipo->list(),JSON_UNESCAPED_UNICODE);
        }
        break;
    case 'POST':
        if($url === 'create'){
            
            $tipo = new Tipo_produtos();
            $tipo_dao = new Tipo_produtosDAO();
            $tipo->setPorcentagem($_REQUEST['porcentagem']);
            $tipo->setNome($_REQUEST['nome']);
            $tipo_dao->create($tipo);
        }else if( $url === 'update'){
            
            $tipo = new Tipo_produtos();
            $tipo_dao = new Tipo_produtosDAO();
            var_dump($_REQUEST);
            die();
            $tipo->setPorcentagem($_REQUEST['porcentagem']);
            $tipo->setNome($_REQUEST['nome']);
            $tipo->setID($_REQUEST['id']);
            $tipo_dao->update($tipo);
            
        }else if($url === 'delete'){
            $tipo = new Tipo_produtos();
            $tipo_dao = new Tipo_produtosDAO();
            $tipo->setPorcentagem($_REQUEST['porcentagem']);
            $tipo->setId($_REQUEST['id']);
            $tipo_dao->delete($tipo->getId());
        }
        break;
    default:
        echo json_encode(['error' => 'método invalido!'],JSON_UNESCAPED_UNICODE);
        http_response_code(404);
        break;
    
}
