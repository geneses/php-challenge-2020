<?php

require_once '../../vendor/autoload.php';

use App\DAO\ProdutosDAO;
use App\Classes\Produtos;

//Recuperando a operação do método
$url = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/')+24);
//Recuperando o método
$metodo = $_SERVER['REQUEST_METHOD'];


//tratar por produto de requisição
switch($metodo){
    case 'GET':
        if($url === 'list'){
            $produto = new ProdutosDAO;
            echo json_encode($produto->list(),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(['error' => 'opção inválida!'],JSON_UNESCAPED_UNICODE);
            http_response_code(404);
        }
        break;
    case 'POST':
        if($url === 'create'){
            $produto = new Produtos();
            $produto_dao = new ProdutosDAO();
            $produto->setNome($_REQUEST['nome']);
            $produto->setValor($_REQUEST['valor']);
            $produto->setTipoProduto($_REQUEST['tipo_produto']);
            $produto_dao->create($produto);
        }else if( $url === 'update'){
            $produto = new Produtos();
            $produto_dao = new ProdutosDAO();
            $produto->setNome($_REQUEST['nome']);
            $produto->setValor($_REQUEST['valor']);
            $produto->setTipoProduto($_REQUEST['tipo_produto']);
            $produto_dao->update($produto);
        }else if($url === 'delete'){
            $produto = new Produtos();
            $produto_dao = new ProdutosDAO();
            $produto->setId($_REQUEST['id']);
            $produto_dao->delete($produto->getId());
        }
        else{
            echo json_encode(['error' => 'opção inválida!'],JSON_UNESCAPED_UNICODE);
            http_response_code(404);
        }
        break;
    default:
        echo json_encode(['error' => 'método invalido!'],JSON_UNESCAPED_UNICODE);
        http_response_code(404);
        break;
    
}


