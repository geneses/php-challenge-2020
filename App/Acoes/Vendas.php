<?php

require_once '../../vendor/autoload.php';

use App\DAO\VendasDAO;
use App\Classes\Vendas;

//Recuperando a operação do método
$url = substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '/')+22);
//Recuperando o método
$metodo = $_SERVER['REQUEST_METHOD'];


//tratar por venda de requisição
switch($metodo){
    case 'GET':
        if($url === 'list'){//retornar todos os itens
            $venda = new VendasDAO;
            echo json_encode($venda->list(),JSON_UNESCAPED_UNICODE);
        }
        else if( $url === 'totalizador'){ //retorna totalizadores
            $venda = new VendasDAO;
            echo json_encode($venda->totalizador(),JSON_UNESCAPED_UNICODE);
        }
        else{
            echo json_encode(['error' => 'opção inválida!'],JSON_UNESCAPED_UNICODE);
            http_response_code(404);
        }
        break;
    case 'POST':
        if($url === 'create'){
            $venda = new Vendas();
            $venda_dao = new VendasDAO();
            $venda->setQuantidade($_REQUEST['quantidade']);
            $venda->setProduto($_REQUEST['produto']);
            $venda_dao->create($venda);
        }
        else{
            echo json_encode(['error' => 'opção inválida!'],JSON_UNESCAPED_UNICODE);
            http_response_code(404);
        }
        break;
    default:
        echo json_encode(['error' => 'método invalido!'],JSON_UNESCAPED_UNICODE);
        http_response_code(404);
        break;
    
}


