
$(document).ready(function ($) {
    
    var url_base = 'App/Acoes/Vendas.php/';

    //botões
    /*function botoes(dados){
        btnEditar = "<button type='button' class='btn btn-default btnEditarTipo' data-id='"+dados.id+"' data-porcentagem='"+dados.porcentagem+"' data-nome='"+dados.nome+"'>editar</button>" ;
        btnDelete = " <button type='button' class='btn btn-default btnDeleteTipo' data-id='"+dados.id+"'>deletar</button>"; 
        return btnEditar+btnDelete;
    }*/

    //função que chama a requisição AJAX
    function listAJAX(){        

        $.getJSON(url_base+'list',function(data){
            var html = "";
            //limpar tabela
            $("#Tabela_vendas tbody").html(html);
            $(data).each(function(i,dados){
                html += "<tr>";
                html += "<td>"+dados.id+"</td><td>"+dados.valor_quantidade+"</td><td>"+dados.valor+"</td><td>"+dados.Valor_inpostos+"</td><td>"+dados.Total_compras+"</td><td>"+dados.porcentagem+"</td>";
                html += "</tr>";
            });
            
            //inserindo no html
            $("#Tabela_vendas tbody").html(html);
        })
    }

       
    //Ação no botão cadastrar
    /*$("#add_tipo_produto").click(function () {
        $("#form_tipo_produto")[0].reset()
        $("#alerta").prop("hidden", true)
        $("#tipo_requisicao").val('create');
        $("#modal_tipo_produto").modal("show");
    });*/

    listAJAX()
    //preencher a tabela
    $("#vendas").click(function(){
        listAJAX()
    })

    

    //Ação Ajax
     $("#form_tipo_produto").submit(function (e) {
         e.preventDefault();
        var valida = true;
        //Validar dados
         $(this).find("input").each(function (i) {
             if ($(this).val() === '') {
                 $("#alerta").prop("hidden", false)
                 $("#alerta").text("Valor " + $(this).attr("id") + " Obrigatório");
                 valida = false;
             }
         })
         if(!valida)
            return false;
         //Dados do form
         var dados = new FormData($("#form_tipo_produto")[0]);
         
  
         $.ajax({
             type: 'post',
             url: url_base+'/create',
             data: dados,
             processData: false,
             contentType: false,
             success: function(data){
                window.location.reload();
             },
             error: function(data){
                 console.log(data);
             }
        })
     });
});